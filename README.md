# Recipe Register

These steps are necessary to get your application up and running.

### What is this repository for?

This is my [Full Stack Web Development Specialization Capstone Project](https://www.coursera.org/learn/web-development-project), a Recipe Register Application.

* Quick summary see: [Ideation report](./1-ideation/ideation-report.pdf)

### How do I get set up?

#### 1. Install required sofware  
1.1. NodeJS  
1.1. NPM  
1.2. Bower  
1.3. ...etc.  
#### 2. Clone this git repo  
#### 3. Download  needed components
```
cd <repo>/recipe-register
cd /rest-server  
npm install
cd ..
```
```
cd web-app  
npm install
bower install
cd ..
```
Create properly signed certificates to your server. See details in `rest-server/bin/letsencrypt.md` file.

#### 4. Start up the spaceship initially

1. Start MongoDB (console 1)  
```
<repo>/recipe-register/mongodb/start-mongodb.sh
```

2. Start REST Server (console 2)
```
cd <repo>/recipe-register/rest-server  
npm start
```

3. Create initial database content (console 3)
 ```
 <repo>/recipe-register/mongodb/init-mongodb-documents.sh
 ```

4. Create initial users (reuse console 3)
```
curl \
    -X POST \
    --insecure \
    -H "Content-Type: application/json" \
    -H "Cache-Control: no-cache" \
    -d '{
            "username": "user",
            "password": "pass"
        }' \
    "https://localhost:3443/api/users/register/"
```
```
curl \
    -X POST \
    --insecure \
    -H "Content-Type: application/json" \
    -H "Cache-Control: no-cache" \
    -d '{
            "username": "admin",
            "password": "pass"
        }' \
    "https://localhost:3443/api/users/register/"
```
```
mongo localhost:27017/conFusion --eval 'db.getCollection("users").update({"username":"admin"},{$set:{"admin":true}});'

```
5. Start Web Client
```
cd <repo>/recipe-register/web-app
gulp watch
```
Note: a refresh on the opened web page may be nedded

6. Start Mobile Client 
```
cd <repo>/recipe-register/mobile-app
ionic run --livereload --consolelong --severlogs android
```
#### 5. Following starts
```
./bootup.sh
```

####  Dependencies
* Install [Node.js](https://nodejs.org/en/) 
* Install some of node packages globally...
```
$ npm list -g --depth 0
/home/zsolt/npm-global/lib
├── bower@1.7.9
├── cordova@6.3.0
├── express-generator@4.13.4
├── generator-angular@0.15.1
├── generator-karma@2.0.0
├── grunt-cli@1.2.0
├── gulp@3.9.1
├── ionic@1.7.16
├── jshint@2.9.2
├── json-server@0.8.17
├── karma-cli@1.0.1
├── kerberos@0.0.21
├── less@2.7.1
├── loopback-datasource-juggler@2.47.0
├── loopback-ds-timestamp-mixin@3.4.0
├── prompt@1.0.0
├── protractor@4.0.2
├── strongloop@6.0.1
└── yo@1.8.4
```

### Who do I talk to?
zsolt.szikora @ gmail.com