#!/usr/bin/env bash
# Note: idea from http://askubuntu.com/a/31004/33108

# MongoDB
gnome-terminal -e "bash -c \" \
    echo -en '${debian_chroot:+($debian_chroot)}\\u@\\h \\w\\a$ '; \
    echo -en '\\033]0;MongoDB\\a'; \
    mongodb/start-mongodb.sh; \
    exec bash\""

read

# REST Server
gnome-terminal -e "bash -c \" \
    bash -c \
    echo -en '${debian_chroot:+($debian_chroot)}\\u@\\h \\w\\a$ '; \
    echo -en '\\033]0;REST_Server\\a'; \
    cd rest-server/; \
    ( npm run-script start | tee logs.log ) ; \
    exec bash\""

read

# Web Client
gnome-terminal -e "bash -c \" \
    echo -en '${debian_chroot:+($debian_chroot)}\\u@\\h \\w\\a$ '; \
    echo -en '\\033]0;Web Client\\a'; \
    cd web-app/; \
    gulp watch; \
    exec bash\""

read

# Mobile Client
gnome-terminal -e "bash -c \" \
    echo -en '${debian_chroot:+($debian_chroot)}\\u@\\h \\w\\a$ '; \
    echo -en '\\033]0;Mobile Client\\a'; \
    cd mobile-app/; \
    ionic run --livereload --consolelong --severlogs android; \
    exec bash\""
