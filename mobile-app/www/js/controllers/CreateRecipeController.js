'use strict';

angular.module('recipeRegister')

    .controller('CreateRecipeController', ['$scope', '$state', '$stateParams', '$ionicHistory',
        function ($scope, $state, $stateParams, $ionicHistory) {

        // TODO implement me
        $scope.goHome = function () {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.home')
        }

        }]);
