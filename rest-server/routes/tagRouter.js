var express = require('express');
var bodyParser = require('body-parser');

var Tags = require('../models/tags');
var TagUsages = require('../models/tagUsages');
var Recipes = require('../models/recipes');
var Verify = require('./verify');
var utils = require('./utils');

var router = express.Router();
router.use(bodyParser.json());

var deleteFoundTags = function (err, tags) {
    if (err) return next(err);
    // pull tag references from all recipes refecencig to this tag

    // 1. collect tagIds and tagUsageId's to be deleted
    var tagIds = tags.map(function (tag) {
        return tag._id;
    });
    var tagUsageIds = tags.map(function (tag) {
        return tag.usage._id;
    })

    // 2. collect all recipe _id's referencing to one of tags to be deleted
    var recipeIds = [].concat.apply([],
        tags.map(function (tag) {
            return tag.usage;
        }).map(function (tagUsage) {
            return tagUsage.taggedRecipes;
        }));
    // TODO remove repeating entries

    // 3. pull references to tagIds from recipes
    Recipes.findAndModify(
        /* query    */ {'_id': {$in: recipeIds}},
        /* sort     */ [],
        /* doc      */ {$pullAll: {recipes: tagIds}},
        /* options  */ {},
        /* callback */ function (err) {
            if (err) return next(err);

            // 4. remove the usages
            TagUsages.remove({'_id': {$in: tagUsageIds}}).exec(function (err) {
                if (err) return next(err);

                // 5. remove the tags
                Tags.remove({'_id': {$in: tagIds}}).exec(function (err, resp) {
                    if (err) return next(err);
                    res.json(resp)
                });
            });
        }
    );
};

// ======================== <tags-path>/ ========================
router.route('/')

    .get( // all tags are public
        function (req, res, next) {
            Tags.find(
                req.query,
                'name useCount', // return only name and useCount properties
                function (err, tags) {
                    if (err) return next(err);
                    res.json(tags);
                });
        })

    .post(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            // 1. create tag usage
            TagUsages.create({'taggedRecipes': []},
                function (err, createdTagUsage) {
                    if (err) return next(err);
                    // 2. create the tag itself
                    Tags.create({
                        name: req.body.name,
                        usage: createdTagUsage._id,
                        createdBy: req.decoded._id
                    }, function (err, createdTag) {
                        if (err) return next(err);
                        res.json(createdTag)
                    })
                });

        })

    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, // only admins can remove tags
        function (req, res, next) {
            Tags.find(req.query)
                .populate('usage')
                .exec(deleteFoundTags(err, tags))
        });

// ======================== <tags-path>/:tagName ========================
router.route('/:tagName')

    .get( // each tag and its usage is also public
        function (req, res, next) {
            Tags.findOne({name: req.params.tagName})
                .populate('createdBy')
                .populate('usage')
                .exec(function (err, tags) {
                    if (err) return next(err);
                    res.json(tags);
                });

        })

    //    .put(utils.notImplemented(req, res, next)) // should NEVER be implemented!

    .delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, // only admins can remove tags
        function (req, res, next) {
            Tags.find({name: req.query.name})
                .populate('usage')
                .exec(deleteFoundTags(err, tags));
        });

module.exports = router;
