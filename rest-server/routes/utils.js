var Utils = function() {

    var notImplemented= function (req, res, next) {
        var err = new Error('Not implemented');
        err.status = 501;
        return next(err);
    };

    return {
        notImplemented: notImplemented
    }
}();

module.exports = Utils;