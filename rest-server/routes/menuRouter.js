var express = require('express');
var bodyParser = require('body-parser');

var Menus = require('../models/menus');
var Recipes = require('../models/recipes');
var Verify = require('./verify');
var utils = require('./utils');

var router = express.Router();
router.use(bodyParser.json());

// ======================== <menus-path>/ ========================
router.route('/')

    .get(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Menus.find(
                {
                    $and: [
                        req.query,
                        // just own or public menus can be queried
                        {
                            $or: [
                                {createdBy: req.param._id},
                                {private: false}
                            ]
                        }

                    ]
                },
                '_id, name createdBy',
                function (err, menus) {
                    if (err) return next(err);
                    res.json(menus);
                })
        }
    )


    .post(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Menus.create({
                name: req.body.name,
                createdBy: req.decoded._id,
                private: req.private,
                recipes: []
            }, function (err, createdMenu) {
                if (err) return next(err);
                res.json(createdMenu)
            });
        })

    .delete(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Menus
                .find(
                    {
                        $and: [
                            req.query,
                            // just owned menus can be queried
                            {createdBy: req.param._id}
                        ]
                    }
                )
                .populate('recipes')
                .exec(function (err, menus) {
                    if (err) return next(err);
                    // pull menu references from all recipes refecencig to this menu

                    // 1. collect menuIds
                    var menuIds = menus.map(function (menu) {
                        return menu._id;
                    });

                    // 2. collect all recipe _id's referencing to one of menus to be deleted
                    var recipeIds = [].concat.apply([],
                        menus.map(function (menu) {
                            return menu.recipes;
                        }).map(function (recipe) {
                            return recipe._id;
                        }));
                    // TODO remove repeating entries

                    // 3. pull references to menuIds from recipes
                    Recipes.findAndModify(
                        /* query    */ {'_id': {$in: recipeIds}},
                        /* sort     */ [],
                        /* doc      */ {$pullAll: {recipes: menuIds}},
                        /* options  */ {},
                        /* callback */ function (err) {
                            if (err) return next(err);

                            // 4. remove the menus
                            Menus.remove({'_id': {$in: menuIds}}).exec(function (err, resp) {
                                if (err) return next(err);
                                res.json(resp)
                            });
                        }
                    );
                })
        });

// ======================== <menus-path>/:menuName ========================
router.route('/:id')

    .get(function (req, res, next) {
        Menus.findOneById(req.params.id)
            .populate('createdBy')
            .poplulate('recipes')
            .exec(function (err, menus) {
                if (err) return next(err);
                res.json(menus);
            });

    })

    //    .put(utils.notImplemented) // should NEVER be implemented!

    .delete(utils.notImplemented); // TODO implement me

module.exports = router;
