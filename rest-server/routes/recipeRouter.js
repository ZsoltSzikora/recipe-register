var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var fs = require('fs');
var path = require('path');
var IMAGE_PATH = path.join(__dirname, '../public/images/'); // TODO make it part of config
var upload = multer({dest: IMAGE_PATH});

/*
 var multerForImageUpload = function () {
 return multer({dest: __dirname + '/public/uploads'});
 }
 */

var Recipes = require('../models/recipes');
var Verify = require('./verify');
var utils = require('./utils');

var router = express.Router();
router.use(bodyParser.json());

var removeManagedCollections = function (req) {
    if (req.body.tags) delete req.body.tags;
    if (req.body.menus)  delete req.body.menus;
};

// ======================== <recipes-path>/ ========================
router.route('/')

    .get( // all recipes are public
        function (req, res, next) {
            Recipes.find(
                (req.query.textPattern)
                    ? {$text: {$search: req.query.textPattern}}
                    : req.query
            )
                .limit(20) // TODO introduce paging
                .populate('createdBy')
                .populate('tags')
                .populate('menus')
                .populate('comments.createdBy')
                .exec(function (err, recipe) {
                    if (err) {
                        return next(err);
                    }
                    res.json(recipe);
                });
        })

    .post(Verify.verifyOrdinaryUser, // logged in users can post recipes
        function (req, res, next) {
            removeManagedCollections(req);
            req.body.createdBy = req.decoded._id;
            Recipes.create(req.body, function (err, recipe) {
                if (err) {
                    return next(err);
                }
                res.json(recipe);
            })
        })

    .delete(Verify.verifyOrdinaryUser, // ordinary users can only remove all THEIR recipes
        function (req, res, next) {
            Recipes.find(req.query).exec(function (err, recipes) {
                if (err) {
                    return next(err);
                }

                var deletable = true;
                for (var i = 0; i < recipes.length; i++) {
                    if (recipes[i].createdBy !== req.decoded._id && !req.decoded.admin) {
                        deletable = false;
                    }
                }

                if (!deletable) {
                    var err = new Error('You are not authorized to perform this operation');
                    err.status = 403;
                    return next(err);
                } else {
                    // TODO populate tags and update tagUsage if needed
                    // TODO populate menus and update menuUsage if needed

                    Recipes.remove(req.query, function (err, resp) {
                        if (err) {
                            return next(err);
                        }
                        // remove images of deleted recipes from file system too...
                        var j, rec;
                        for (j = 0; j < recipes.length; j++) {
                            rec = recipes[j];
                            rec.images.forEach(function (img) {
                                fs.unlink(path.join(IMAGE_PATH, img), function (exception) {
                                    if (exception) next(exception);
                                });
                            });
                        }
                        res.json(resp);
                    });
                }
            })
        });

// ======================== <recipes-path>/:id ========================
router.route('/:id')

    .get( // all recipes are public
        function (req, res, next) {
            Recipes.findOneAndUpdate(
                {_id: req.params.id},
                {$inc: {viewCount: 1}}, // increment viewCount now!
                {new: true}
            )
                .populate('createdBy')
                .populate('tags')
                .populate('menus')
                .populate('comments.createdBy')
                .exec(function (err, recipe) {
                    if (err) {
                        return next(err);
                    }
                    res.json(recipe);
                })
        })

    .put(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            removeManagedCollections(req);
            Recipes.findById(req.params.id)
                .exec(function (err, recipe) {
                    if (err) {
                        return next(err);
                    }
                    if (!recipe || recipe.createdBy.toString() !== req.decoded._id && !req.decoded.admin) {
                        var err2 = new Error('You are not authorized to perform this operation');
                        err2.status = 403;
                        return next(err2);
                    }
                    Recipes.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, updatedRecipe) {
                        if (err) {
                            return next(err);
                        }
                        // images removed from the recipe should also be removed from file system...
                        recipe.images.forEach(function (img) {
                            var imgMissingFromUpdatedRecipe= (updatedRecipe.images.indexOf(img) === -1);
                            if  (imgMissingFromUpdatedRecipe) {
                                fs.unlink(path.join(IMAGE_PATH, img), function (exception) {
                                    if (exception) next(exception);
                                })
                            }
                        });
                        // TODO return here with a pupulated recipe
                        res.json(updatedRecipe);
                    })
                })
        })

    .delete(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Recipes.findById(req.params.id)
                .exec(function (err, recipe) {
                    if (err) {
                        return next(err);
                    }
                    if (!recipe || recipe.createdBy.toString() !== req.decoded._id && !req.decoded.admin) {
                        var err = new Error('You are not authorized to perform this operation');
                        err.status = 403;
                        return next(err);
                    }
                    Recipes.remove(req.params.id, function (err, resp) {
                        if (err) {
                            return next(err);
                        }
                        // TODO populate tags and update tagUsage if needed
                        // TODO menus and update menuUsage if needed
                        res.json(resp);
                    })
                })
        });

// ======================== <recipes-path>/:id/images ========================
router.route('/:id/images')

    .get(utils.notImplemented)

    .post(Verify.verifyOrdinaryUser, // logged in users can post recipe images
        upload.any(),
        function (req, res, next) {
            console.log('POST RECIVED');
            var i;
            var newlyAddedFileNames = [];
            for (i = 0; i < req.files.length; i++) {
                newlyAddedFileNames.push(req.files[i].filename);
            }
            Recipes.update({_id: req.params.id}, {$push: {images: {$each: newlyAddedFileNames}}})
                .exec(function (err, other) {
                    if (err) {
                        return next(err);
                    }
                    console.log('Recipe ' + req.params.id + ' got new images: ' + newlyAddedFileNames.toString());
                    res.json({success: true, fileCount: req.files.length});
                });

        })

    .delete(utils.notImplemented);


// ======================== <recipes-path>/:id/comments ========================
router.route('/:id/comments')

    .get( // all comments are public
        function (req, res, next) {
            Recipes.findById(req.params.id)
                .populate('comments.createdBy')
                .exec(function (err, recipe) {
                    if (err) {
                        return next(err);
                    }
                    res.json(recipe.comments);
                });
        })
    .post(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Recipes.findById(req.params.id, function (err, recipe) {
                if (err) {
                    return next(err);
                }
                req.body.createdBy = req.decoded._id;
                recipe.comments.push(req.body);
                recipe.save(function (err, savedRecipe) {
                    if (err) {
                        return next(err);
                    }
                    res.json(savedRecipe.comments[savedRecipe.comments.length - 1]);
                })
            });
        })

    .delete(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Recipes.findById(req.params.id, function (err, recipe) {
                if (err) {
                    return next(err);
                }
                // only recipe owner or admin can remove all comments
                if (recipe && recipe.createdBy !== req.decoded._id && !req.decoded.admin) {
                    var err = new Error('You are not authorized to perform this operation');
                    err.status = 403;
                    return next(err);
                }
                for (var i = recipe.comments.length - 1; i >= 0; --i) {
                    recipe.commments.id(recipe.commments[i]._id).remove();
                }
                recipe.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.writeHead(200, {'Content-Type': 'text/plain'});
                    res.end('Deleted all comments!');
                })
            });

        });

// ======================== <recipes-path>/:recipeId/comments/:commentId ========================
router.route('/:recipeId/comments/:commentId')

    .get(function (req, res, next) {
        Recipes.findById(req.params.recipeId)
            .populate('comments.createdBy')
            .exec(function (err, foundRecipe) {
                if (err) {
                    return next(err);
                }
                res.json(foundRecipe.comments.id(req.params.commentId));
            });
    })

    .put(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            // We delete the existing comment and insert the updated comment as a new comment
            Recipes.findById(req.params.recipeId, function (err, recipe) {
                if (err) {
                    return next(err);
                }
                // only comment owner or admin can modify comments
                var commenterId = recipe.comments.id(req.params.commentId).createdBy;
                if (commenterId !== req.decoded._id && !req.decoded.admin) {
                    var err = new Error('You are not authorized to perform this operation');
                    err.status = 403;
                    return next(err);
                }
                req.body.createdBy = req.decoded._id;

                recipe.comments.push(req.body);
                recipe.save(function (err, savedRecipe) {
                    if (err) {
                        return next(err);
                    }
                    console.log('Updated Comments!');
                    res.json(savedRecipe.comments.id(req.params.commentId));
                });
            });
        })

    .delete(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Recipes.findById(req.params.recipeId, function (err, recipe) {
                if (err) {
                    return next(err);
                }
                // only comment owner or admin can remove comments
                var commenterId = recipe.comments.id(req.params.commentId).createdBy;
                if (commenterId !== req.decoded._id && !req.decoded.admin) {
                    var err = new Error('You are not authorized to perform this operation');
                    err.status = 403;
                    return next(err);
                }
                recipe.comments.id(req.params.commentId).remove();
                recipe.save(function (err, savedRecipe) {
                    if (err) {
                        return next(err);
                    }
                    res.json(savedRecipe.comments.id(req.params.commentId));
                });
            });
        });


// ======================== <recipes-path>/:id/tags ========================
router.route('/:id/tags')

    .get(utils.notImplemented) // TODO get all tag names for this recipe

    .post(utils.notImplemented) // TODO add a new tag to this recipe by name

    .delete(utils.notImplemented); // TODO remove all tags from this recipe

// ======================== <recipes-path>/:recipeId/tags/tagName ========================
router.route('/:recipeId/tags/:tagName')

    .get(utils.notImplemented) // TODO get detailed tag info about this tag

    // .put(utils.notImplemented) // should NEVER be implemented!

    .delete(utils.notImplemented); // TODO tag from the recipe by name

// ======================== <recipes-path>/:id/menus ========================
router.route('/:id/menus')

    .get(utils.notImplemented) // TODO get all acessible menus containing this recipe

    .post(utils.notImplemented) // TODO this recipe to a new acessible menu by _ID

    .delete(utils.notImplemented); // TODO remove this recipe from all accessible menus

// ======================== <recipes-path>/:recipeid/menus/:menuId ========================
router.route('/:recipeId/menus/:menuId')

    .get(utils.notImplemented) // TODO get menu data

    // .put(utils.notImplemented) // should NEVER be implemented!

    .delete(utils.notImplemented); // TODO remove recipe from menu _ID

// ---

module.exports = router;