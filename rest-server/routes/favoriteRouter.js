var express = require('express');
var bodyParser = require('body-parser');

var Favorites = require('../models/favorites');
var Verify = require('./verify');

var router = express.Router();
router.use(bodyParser.json());


// ======================== <favorites-path>/ ========================
router.route('/')

    .get(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Favorites.findOne({postedBy: req.decoded._id})
                .populate('postedBy')
                .populate('recipes')
                .exec(function (err, favorite) {
                    if (err) return next(err);
                    res.json(favorite);
                });
        })

    .post(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            var recipeObjectId = req.body._id;
            console.log(JSON.stringify(req.decoded));
            Favorites.findOne({postedBy: req.decoded._id})
                .exec(function (err, favorite) {
                    if (err) return next(err);
                    if (!favorite) {
                        Favorites.create({
                            postedBy: req.decoded._id,
                            recipes: [recipeObjectId]
                        }, function (err, createdFavorite) {
                            if (err) return next(err);
                            res.json(createdFavorite)
                        })
                    } else {
                        favorite.recipes.push(recipeObjectId);
                        favorite.save(function (err, savedFavorite) {
                            if (err) return next(err);
                            res.json(savedFavorite);
                        });
                    }
                });
        })

    .delete(Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Favorites.findOneAndRemove({postedBy: req.decoded._id}, function (err, resp) {
                if (err) return next(err);
                res.json(resp);
            });
        });

// ======================== <favorites-path>/:recipeObjectId ========================

router.route('/:recipeObjectId')

    .delete(
        Verify.verifyOrdinaryUser,
        function (req, res, next) {
            Favorites.findOne({postedBy: req.decoded._id})
                .exec(function (err, favorite) {
                    if (err) return next(err);
                    if (!favorite) {
                        return res.status(404).json({
                            err: 'There are no favorite recipes for user ' + req.decoded.username
                        });
                    } else {
                        var index = favorite.recipes.indexOf(req.params.recipeObjectId);
                        if (index === -1) {
                            return res.status(404).json({
                                err: 'Recipe with id ' + req.params.recipeObjectId + ' is not a favorite for user ' + req.decoded.username
                            });
                        } else {
                            favorite.recipes.splice(index, 1);
                            if (favorite.recipes.length > 0) {
                                // some recipes remained in foundFavorite --> persist back foundFavorite
                                favorite.save(function (err, savedFavorite) {
                                    if (err) return next(err);
                                    res.json(savedFavorite);
                                })
                            } else {
                                // mo more recipes remained --> remove the entire foundFavorite document
                                Favorites.remove({postedBy: req.decoded._id}, function (err, resp) {
                                    if (err) return next(err);
                                    res.json(resp);
                                });
                            }
                        }
                    }
                });
        });

module.exports = router;
