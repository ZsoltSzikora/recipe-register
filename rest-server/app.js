var express = require('express');
var cors = require('cors');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var expressListRoutes = require('express-list-routes');

var config = require('./config');

mongoose.connect(config.mongoUrl);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log("Connected correctly to server");
});

var routes = require('./routes/index');
var users = require('./routes/users');

var recipeRouter = require('./routes/recipeRouter');
var favoriteRouter = require('./routes/favoriteRouter');
var tagRouter = require('./routes/tagRouter');
var menuRouter = require('./routes/menuRouter');

var app = express();

// Secure traffic only
app.all('*', function (req, res, next) {
    if (req.secure) {
        return next();
    }
    res.redirect('https://' + req.hostname + ":" + app.get('secPort') + req.url);
});


// CORS setup
var corsOptions = {
    origin: ["localhost", "zsolt.ddns.net"],
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
// include cors before other routes
app.options('*', cors());
app.head('*', cors());
app.get('*', cors());
app.put('*', cors());
app.post('*', cors());
app.delete('*', cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// passport config
app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'public')));

// ======== recipe register ========
app.use(config.serviceRoot, routes);
app.use(config.serviceRoot + '/', routes);
app.use(config.serviceRoot + '/users', users);
app.use(config.serviceRoot + '/recipes', recipeRouter);
app.use(config.serviceRoot + '/favorites', favoriteRouter);
app.use(config.serviceRoot + '/tags', tagRouter);
app.use(config.serviceRoot + '/menus', menuRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler will print stacktrace
if (/* app.get('env') === 'development' */ true) {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

expressListRoutes({prefix: '/api/routes'}, '\nroutes', routes);
expressListRoutes({prefix: '/api/users'}, '\nusers', users);
expressListRoutes({prefix: '/api/recipes'}, '\nrecipes', recipeRouter);
expressListRoutes({prefix: '/api/favorites'}, '\nfavorites', favoriteRouter);
expressListRoutes({prefix: '/api/tags'}, '\ntags', tagRouter);
expressListRoutes({prefix: '/api/menus'}, '\nmenus', menuRouter);

module.exports = app;
