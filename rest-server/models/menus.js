var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var menusSchema = new Schema({
    name: {type: String, required: true},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    private: {type: Boolean, default: true},
    recipes: [{type: Schema.Types.ObjectId, ref: 'Recipe'}]
}, {timestamps: true});

module.exports = mongoose.model('Menu', menusSchema);