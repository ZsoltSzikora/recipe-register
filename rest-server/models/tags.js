var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagsSchema = new Schema({
    name:  {type: String, index: { unique: true }},
    createdBy: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    useCount: {type: Number, default: 0},
    usage: {type: Schema.Types.ObjectId, ref: 'TagUsage'},
}, {timestamps: true});

// enable findAndModify for this schema (used in tagRouter)
tagsSchema.statics.findAndModify = function (query, sort, doc, options, callback) {
    return this.collection.findAndModify(query, sort, doc, options, callback);
};

module.exports = mongoose.model('Tag', tagsSchema);