var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    rating:  {type: Number, min: 1, max: 5, required: true},
    text: {type: String, required: true},
    createdBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
}, {timestamps: true});

var recipesSchema = new Schema({
    createdBy:   {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    name:        {type: String, required: true, unique: true},
    type:        {type: String, enum: ['IMAGE', 'TEXT'], required: true},
    description: {type: String, required: true},
    images:      [ {type: String, required: true} ], // for image repices
    ingredients: [String], // for text recipes
    steps:       [String], // for text recipes
    tags:        [ {type: mongoose.Schema.Types.ObjectId, ref: 'Tag'} ], // tags are public
    menus:       [ {type: mongoose.Schema.Types.ObjectId, ref: 'Menu'} ], // menus are private by default
    comments:    [commentSchema],
    viewCount:   {type: Number, defalult: 0},
}, {timestamps: true});

recipesSchema.index({'$**': 'text'});

module.exports = mongoose.model('Recipe', recipesSchema);