var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tagUsageSchema = new Schema({
    taggedRecipes: [{type: Schema.Types.ObjectId, ref: 'Recipe'}]
}, {timestamps: true});

module.exports = mongoose.model('TagUsage', tagUsageSchema);