var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var favoritesSchema = new Schema({
    postedBy: {type: Schema.Types.ObjectId, ref: 'User'},
    dishes: [{type: Schema.Types.ObjectId, ref: 'Recipe'}]
}, {timestamps: true});

module.exports = mongoose.model('Favorite', favoritesSchema);