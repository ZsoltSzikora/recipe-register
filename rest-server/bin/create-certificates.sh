#!/usr/bin/env bash
DIR="$(cd "$(dirname "$0")" && pwd)"
# create private key (the private key always include the public key too)
openssl genrsa 4096 > $DIR/privkey.pem

# create certificate request for this private key
openssl req -new -key $DIR/private.key -out $DIR/cert.csr

# using own private key sign the certificate request and create a self-signed certificate
openssl x509 -req -in $DIR/cert.csr -signkey $DIR/private.key -out $DIR/fullchain.pem
