# How to Use Let's Encrypt #

### 1. Install Let's Encrypt and ###
https://letsencrypt.org/getting-started/  
https://certbot.eff.org/#ubuntuxenial-other

### 2. Generate private key and sign it ###
```
sudo letsencrypt certonly --standalone
```

### 3. Copy them to Rest Server ###
Just for Captain's log...
```
sudo -s
cp /etc/letsencrypt/live/zsolt.ddns.net/privkey.pem /home/zsolt/repos/bitbucket/recipe-register/rest-server/bin/
cp /etc/letsencrypt/live/zsolt.ddns.net/fullchain.pem /home/zsolt/repos/bitbucket/recipe-register/rest-server/bin/
chown zsolt /home/zsolt/repos/bitbucket/recipe-register/rest-server/bin/privkey.pem
chown zsolt /home/zsolt/repos/bitbucket/recipe-register/rest-server/bin/fullchain.pem
```
### 4. Done :) ###
