You are planning to support commercial IoT device types (Nest, Phillips	smart, light bulbs, WIFI switches, ...). I am not familiar with this IoT domain, but I am happy to hear this market is also going into a direction of some kind of standardization. Or else you could get into trouble with creating the appropriate driver set for managing all these different devices within the capstone's time frame. :-)

I like your idea. Good luck!
