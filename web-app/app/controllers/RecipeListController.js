'use strict';

angular.module('recipeRegisterApp')

    .controller('RecipesListController', ['$scope', 'recipeFactory', 'imagePathFactory', function ($scope, recipeFactory, imagePathFactory) {

        $scope.showRecipes = false;
        $scope.message = "Loading ...";
        $scope.searchStr = '';

        $scope.$watch('searchStr', function (tmpStr) {
            // if searchStr is still the same... go ahead and retrieve the data
            if (tmpStr === $scope.searchStr) {
                $scope.showRecipes = false;
                $scope.message = "Loading ...";
                recipeFactory.query(
                    ($scope.searchStr.length > 0 ) ? {textPattern: $scope.searchStr} : null
                ).$promise.then(
                    function (response) {
                        $scope.recipes = response;
                        $scope.showRecipes = true;
                    }, function (error) {
                        $scope.message = "Error: " + error.status + " " + error.statusText;
                    });
            }
        });

        $scope.getFirstImgSrc = function (recipe) {
            var imgSpec = recipe.images[0] || "placeholder.png";
            return imagePathFactory(imgSpec)
        };

    }]);
