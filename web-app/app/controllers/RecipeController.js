'use strict';

angular.module('recipeRegisterApp')

    .controller('RecipeController', ['$rootScope', '$scope', '$state', '$stateParams', 'recipeFactory', 'commentFactory', 'imagePathFactory', 'AuthFactory', 'ngDialog',
        function ($rootScope, $scope, $state, $stateParams, recipeFactory, commentFactory, imagePathFactory, AuthFactory, ngDialog) {

            $scope.Math = window.Math;
            $scope.recipe = {};
            $scope.showRecipe = false;
            $scope.message = "Loading ...";
            $scope.editEnabled = false;
            $scope.ownedByUser = false;

            $rootScope.$on('login:Successful', function () {
                $scope.ownedByUser = ($scope.recipe.createdBy._id == AuthFactory.getUserId());
                $scope.editEnabled = false;
            });

            $rootScope.$on('logout:Successful', function () {
                $scope.ownedByUser = false;
                $scope.editEnabled = false;
            });

            recipeFactory.get({
                id: $stateParams.id
            }).$promise.then(
                function (response) {
                    $scope.recipe = response;
                    $scope.showRecipe = true;
                    $scope.ownedByUser = ($scope.recipe.createdBy._id == AuthFactory.getUserId());
                }, function (error) {
                    $scope.message = "Error: " + error.status + " " + error.statusText;
                });

            $scope.mycomment = {
                rating: 5,
                comment: ""
            };

            $scope.doUpdate = function () {
                $scope.recipe.$update()
                    .then(
                        function (response) {
                            $scope.recipe = response;
                            $scope.editEnabled = false;
                        }, function (error) {
                            $scope.showRecipe = false;
                            $scope.message = "Error: " + error.status + " " + error.statusText;
                        });
            }

            $scope.setEditEnabled = function (value) {
                $scope.editEnabled = value;
            }

            $scope.reloadPage = function () {
                $state.go($state.current, {}, {reload: true});
            }

            $scope.submitComment = function () {
                commentFactory.save({id: $stateParams.id}, $scope.mycomment);
                $scope.reloadPage();
                // $scope.commentForm.$setPristine();
                $scope.mycomment = {
                    rating: 5,
                    comment: ""
                };
            };

            $scope.imgSrc = function (suffix) {
                var imgSpec = suffix || "placeholder.png";
                return imagePathFactory(imgSpec);
            };

            $scope.addIngredient = function () {
                $scope.recipe.ingredients.push("");
            }

            $scope.removeIngredient = function (index) {
                $scope.recipe.ingredients.splice(index, 1);
            }

            $scope.addStep = function () {
                $scope.recipe.steps.push("");
            }

            $scope.removeStep = function (index) {
                $scope.recipe.steps.splice(index, 1);
            }

            $scope.removeComment = function (index) {
                $scope.recipe.comments.splice(index, 1);
            }

            $scope.removeImage = function (index) {
                $scope.recipe.images.splice(index, 1);
            }

            $scope.range = function (min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = min; i <= max; i += step) {
                    input.push(i);
                }
                return input;
            };

            $scope.openImageUpload = function () {
                ngDialog.open({
                    template: 'views/image-upload.html',
                    scope: $scope,
                    className: 'ngdialog-theme-default',
                    controller: "ImageUploadController"
                });
            };

        }])
    .controller('ImageUploadController', ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http, $timeout) {

        $scope.interface = {};
        $scope.uploadCount = 0;
        $scope.success = false;
        $scope.error = false;

        // Listen for when the interface has been configured.
        $scope.$on('$dropletReady', function whenDropletReady() {

            $scope.interface.allowedExtensions(['png', 'jpg', 'bmp', 'gif', 'svg']);
            $scope.interface.setRequestUrl('https://zsolt.ddns.net:3443/api/recipes/' + $scope.recipe._id + '/images');
            $scope.interface.setRequestHeaders({'x-access-token': $http.defaults.headers.common['x-access-token']});
            $scope.interface.defineHTTPSuccess([/2.{2}/]);
            $scope.interface.useArray(false);

        });

        // Listen for when the files have been successfully uploaded.
        $scope.$on('$dropletSuccess', function onDropletSuccess(event, response, files) {

            $scope.uploadCount = files.length;
            $scope.success = true;
            ngDialog.close();
            console.log(response, files);
            $scope.reloadPage();

            window.setTimeout(function timeout() {
                $scope.success = false;
            }, 5000);

        });

        // Listen for when the files have failed to upload.
        $scope.$on('$dropletError', function onDropletError(event, response) {

            $scope.error = true;
            console.log(response);

            window.setTimeout(function timeout() {
                $scope.error = false;
            }, 5000);

        });


    }]);


