'use strict';

var app = angular.module(
    'recipeRegisterApp', [
        'ui.router',
        'ngResource',
        'ngDialog',
        'xeditable',
        'ngDroplet'
    ]);

app.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'views/home.html',
                        controller  : 'RecipesListController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html'
                    }
                }

            })

            // route for the recipe details
            .state('app.recipe', {
                url: 'recipe/:id',
                views: {
                    'content@': {
                        templateUrl : 'views/recipe.html',
                        controller  : 'RecipeController'
                    }
                }
            })


            // route for QR Menus page
            .state('app.menus', {
                url:'menus',
                views: {
                    'content@': {
                        templateUrl : 'views/qr-menus.html',
                        controller  : 'MenuListController'
                    }
                }
            })

            // route for the settings page
            .state('app.settings', {
                url:'settings',
                views: {
                    'content@': {
                        templateUrl : 'views/settings.html',
                        controller  : 'SettingsController'
                    }
                }
            })

            // route for the about page
            .state('app.about', {
                url:'about',
                views: {
                    'content@': {
                        templateUrl : 'views/about.html',
                        controller  : 'AboutController'
                    }
                }
            })


    $urlRouterProvider.otherwise('/');
    })
;

app.config(function($provide) {
        $provide.decorator('$state', function($delegate, $stateParams) {
            $delegate.forceReload = function() {
                return $delegate.go($delegate.current, $stateParams, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            };
            return $delegate;
        });
    });

app.run(function(editableOptions) {
    editableOptions.theme = 'bs3';
});

